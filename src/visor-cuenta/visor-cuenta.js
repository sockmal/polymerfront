import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuenta extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          // all: initial;
          border: solid blue;
        }
      .redbg {
        background-color: red;
      }
      .greenbg {
        background-color: green;
      }
      .bluebg {
        background-color: blue;
      }
      .greybg {
        background-color: green;
      }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <template is="dom-repeat" items="{{accountResponse}}">
        <div><br/># [[index]]</div><div>IBAN Number: <span>[[item.IBAN]]</span></div><div>Balance: <span>[[item.balance]]</span></div><div>userID: <span>[[item.idUsuario]]</span></div>
      </template>

      <h2>Mi cuenta es [[IBAN]]</h2>
      <h2>mi balance es [[balance]]</h2>
      <h2>mi userid es [[idUsuario]]</h2>

      <iron-ajax
        auto
        id="getAccount"
        url="http://localhost:3000/apitechu/v2/account/{{id::input}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      }, last_name: {
        type: String
      }, email: {
        type: String
      }, id: {
        type: Number
      }, IBAN: {
        type: String
      }, balance: {
        type: Number
      }, idUsuario: {
        type: Number
      }, accountResponse: {
        type: Array
      }
    };
  } // End properties

  showData (data){
    console.log("showData");

    console.log(data.detail.response);
    console.log(data.detail.response[0].IBAN);

    this.accountResponse = data.detail.response;
    this.IBAN = data.detail.response[0].IBAN;
    this.balance = data.detail.response[0].balance;
    this.idUsuario = data.detail.response[0].idUsuario;
  }

} // End Class

window.customElements.define('visor-cuenta', VisorCuenta);
