import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../login-usuario/login-usuario.js';
import '../visor-usuario/visor-usuario.js';

/**
 * @customElement
 * @polymer
 */
class UserDashboard extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h1>Yo soy tu padre</h1>
      <login-usuario on-myevent="processEvent"></login-usuario>
      <visor-usuario id="receiver"></visor-usuario>
    `;
  }
  static get properties() {
    return {
    };
  } // End Properties
  processEvent(e) {
    console.log("Capturado evento del emisor");
    console.log(e);

    this.$.receiver.idusuario = e.detail.idusuario;
  }
} // End Class

window.customElements.define('user-dashboard', UserDashboard);
